package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	godotenv "github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	loadEmojiJson()

	apiKey := os.Getenv("TG_BOT_API_KEY")
	bot, err := tgbotapi.NewBotAPI(apiKey)

	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "help":
				msg.Text = "Use /hewwow for something cool."
			case "hewwow":
				msg.Text = "👁👄👁"
			default:
				msg.Text = "I don't understand."
			}
		} else {
			msg.Text = EmojifyText(update.Message.Text)
		}

		if _, err := bot.Send(msg); err != nil {
			log.Panic(err)
		}
	}
}

func loadEmojiJson() {
	jsonFile, err := os.Open("emojis.json")

	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteValue, &emojis)
}
