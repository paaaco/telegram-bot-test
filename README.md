# PacoBot

This is an admittedly very dumb Telegram Bot that echoes what you tell it, but with an overabundance of emoji.

Right now, it currently exists as a bot on Telegram named `@paaaco_bot`. It may or may not be running live at this moment.

![Screenshot of Telegram conversation with the bot](/docs/images/screenshot.png "Screenshot")