package main

import (
	"testing"
)

func TestGetEmoji(t *testing.T) {
	loadEmojiJson()
	gotEmojiChar := GetEmoji("cat")
	t.Log(gotEmojiChar)

	expectedEmojiChars := []string{"🦁", "🐯", "🐅", "🐆", "🐈"}

	if !contains(expectedEmojiChars, gotEmojiChar) {
		t.Errorf("%q is not inside list of expected emoji %q", gotEmojiChar, expectedEmojiChars)
	}
}

func TestEmojifyText(t *testing.T) {
	loadEmojiJson()
	gotText := EmojifyText("hello")
	expectedText := []string{"hello✋", "hello🖐️"}

	if !contains(expectedText, gotText) {
		t.Errorf("%q is not the same as expected text %q", gotText, expectedText)
	}

	gotText = EmojifyText("HELLO")
	expectedText = []string{"HELLO✋", "HELLO🖐️"}

	if !contains(expectedText, gotText) {
		t.Errorf("%q is not the same as expected text %q", gotText, expectedText)
	}
}

func contains(strings []string, str string) bool {
	for _, v := range strings {
		if v == str {
			return true
		}
	}

	return false
}
