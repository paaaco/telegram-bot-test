package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"golang.org/x/exp/slices"
)

var emojis []Emoji

type Emoji struct {
	EmojiChar string   `json:"emoji"`
	Name      string   `json:"name"`
	Keywords  []string `json:"keywords"`
}

func GetEmoji(s string) string {
	var emojiCandidates []string
	token := strings.ToLower(s)

	for _, emoji := range emojis {
		if emoji.Name == token || slices.Contains(emoji.Keywords, token) {
			emojiCandidates = append(emojiCandidates, emoji.EmojiChar)
		}
	}

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(emojiCandidates), func(i, j int) {
		emojiCandidates[i], emojiCandidates[j] = emojiCandidates[j], emojiCandidates[i]
	})

	if len(emojiCandidates) <= 0 {
		return ""
	}
	return emojiCandidates[0]
}

func EmojifyText(s string) string {
	tokens := strings.Split(s, " ")
	var emojifiedText []string

	for _, token := range tokens {
		tokenWithEmoji := fmt.Sprintf("%s%s", token, GetEmoji(token))
		emojifiedText = append(emojifiedText, tokenWithEmoji)
	}
	return strings.Join(emojifiedText[:], " ")
}
