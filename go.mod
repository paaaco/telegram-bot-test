module gitlab.com/paaaco/telegram-paco-bot

go 1.18

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	golang.org/x/exp v0.0.0-20220613132600-b0d781184e0d // indirect
)
